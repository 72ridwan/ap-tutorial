package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    public ModelDuck () {
        super.setFlyBehavior(new FlyNoWay());
        super.setQuackBehavior(new Quack());
    }

    public void display() {
        System.out.println("I'm a model duck");
    }
}
