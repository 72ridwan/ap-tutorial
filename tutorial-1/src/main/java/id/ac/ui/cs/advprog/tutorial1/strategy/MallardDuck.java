package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    public MallardDuck(){
        super.setQuackBehavior(new Quack());
        super.setFlyBehavior(new FlyWithWings());
    }

    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
