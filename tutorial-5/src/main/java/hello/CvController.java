package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        if (visitor == null) {
            model.addAttribute("titlePage", "Ini CV Saya");
        } else {
            model.addAttribute("titlePage",
                    visitor + ", Saya harap Anda tertarik mempekerjakan Saya");
        }
        return "cv";
    }

}
