package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class RoseBrandDough implements Dough {

    @Override
    public String toString() {
        return "Special dough made from beloved Rose Brand flour";
    }
}