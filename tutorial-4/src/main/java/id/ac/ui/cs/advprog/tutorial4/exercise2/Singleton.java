package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton instansiUnik = new Singleton();

    private Singleton() {
    }

    public static Singleton getInstance() {
        return instansiUnik;
    }
}
