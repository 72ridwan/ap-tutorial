package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PanturaClams implements Clams {

    @Override
    public String toString() {
        return "Pantura Clams";
    }
}