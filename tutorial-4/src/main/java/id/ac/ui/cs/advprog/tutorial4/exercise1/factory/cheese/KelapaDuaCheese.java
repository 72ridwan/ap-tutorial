package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class KelapaDuaCheese implements Cheese {

    @Override
    public String toString() {
        return "Kelapa Dua Asli Richeese";
    }
}
