package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class ChicagoPizzaStoreTest {

    private ChicagoPizzaStore chicagoPizzaStore;
    private Pizza cheesePizza;
    private Pizza clamPizza;
    private Pizza veggiePizza;


    @Before
    public void setUp() {
        chicagoPizzaStore = new ChicagoPizzaStore();
        cheesePizza = chicagoPizzaStore.createPizza("cheese");
        clamPizza = chicagoPizzaStore.createPizza("clam");
        veggiePizza = chicagoPizzaStore.createPizza("veggie");
    }

    @Test
    public void pizzaStoreDepokGivesAppropriatePizza() {
        cheesePizza.prepare();
        System.out.println(cheesePizza);

        clamPizza.prepare();
        System.out.println(clamPizza);

        veggiePizza.prepare();
        System.out.println(veggiePizza);

        assertEquals(cheesePizza.getName(), "Chicago Style Cheese Pizza");
        assertEquals(clamPizza.getName(), "Chicago Style Clam Pizza");
        assertEquals(veggiePizza.getName(), "Chicago Style Veggie Pizza");
    }

}
