package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    private NewYorkPizzaStore newYorkPizzaStore;
    private Pizza cheesePizza;
    private Pizza clamPizza;
    private Pizza veggiePizza;


    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
        cheesePizza = newYorkPizzaStore.createPizza("cheese");
        clamPizza = newYorkPizzaStore.createPizza("clam");
        veggiePizza = newYorkPizzaStore.createPizza("veggie");
    }

    @Test
    public void pizzaStoreNewYorkGivesAppropriatePizza() {
        cheesePizza.prepare();
        System.out.println(cheesePizza);

        clamPizza.prepare();
        System.out.println(clamPizza);

        veggiePizza.prepare();
        System.out.println(veggiePizza);

        assertEquals(cheesePizza.getName(), "New York Style Cheese Pizza");
        assertEquals(clamPizza.getName(), "New York Style Clam Pizza");
        assertEquals(veggiePizza.getName(), "New York Style Veggie Pizza");
    }

    @Test
    public void newYorkPizzaStoreCanOrderPizza() {
        Pizza pizza = newYorkPizzaStore.orderPizza("cheese");
        assertEquals(cheesePizza.getName(), pizza.getName());

        pizza = newYorkPizzaStore.orderPizza("clam");
        assertEquals(clamPizza.getName(), pizza.getName());

        pizza = newYorkPizzaStore.orderPizza("veggie");
        assertEquals(veggiePizza.getName(), pizza.getName());
    }
}
