package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private DepokPizzaStore depokPizzaStore;
    private Pizza cheesePizza;
    private Pizza clamPizza;
    private Pizza veggiePizza;


    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
        cheesePizza = depokPizzaStore.createPizza("cheese");
        clamPizza = depokPizzaStore.createPizza("clam");
        veggiePizza = depokPizzaStore.createPizza("veggie");
    }

    @Test
    public void pizzaStoreDepokGivesAppropriatePizza() {
        cheesePizza.prepare();
        System.out.println(cheesePizza);

        clamPizza.prepare();
        System.out.println(clamPizza);

        veggiePizza.prepare();
        System.out.println(veggiePizza);

        assertEquals(cheesePizza.getName(), "Depok Style Cheese Pizza");
        assertEquals(clamPizza.getName(), "Depok Style Clam Pizza");
        assertEquals(veggiePizza.getName(), "Depok Style Veggie Pizza");
    }

    @Test
    public void depokPizzaStoreCanOrderPizza() {
        Pizza pizza = depokPizzaStore.orderPizza("clam");
        assertEquals(clamPizza.getName(), pizza.getName());

        pizza = depokPizzaStore.orderPizza("cheese");
        assertEquals(cheesePizza.getName(), pizza.getName());

        pizza = depokPizzaStore.orderPizza("veggie");
        assertEquals(veggiePizza.getName(), pizza.getName());
    }
}
