package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {

    @Test
    public void testPizza() {
        Cheese parmesan = new ParmesanCheese();
        System.out.println(parmesan);
        PizzaTestDrive.main(new String[]{});
    }
}
